# tcolors

Quick and dirty script for seeing terminal colors. Assumes xterm-type
sequences (for now) and 256 color (for now). This makes the script an
excellent way to see if your terminal in fact does have 256 color support,
whether or not it reports the capability.

Downside of this approach is that the script doesn't work under things like
fbterm since the wrong terminal codes get used. Fixing that requires patches
to the code I haven't written yet.

You'll notice the grayscale bit at the bottom includes colors 16 and 231. Most
scripts like this don't repeat them because they've already been displayed.
Well, they're included here.
